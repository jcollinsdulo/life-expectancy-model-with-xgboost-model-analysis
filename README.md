# XGBoost for Supervised Learning
Supervised Learning with XGBoost and data processing with python pandas.

# Why XGBoost
XGBoost allows you to analyze what informed the decisions of the model. This project tries to predict Life Expetancy of a country while also analyzing what (features) informed the model predictions.

